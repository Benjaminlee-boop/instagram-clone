import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyAhx6vT-epSQv1aMY0E-XVwNB9NX06xg2A",
  authDomain: "instagram-clone-bb00e.firebaseapp.com",
  databaseURL: "https://instagram-clone-bb00e.firebaseio.com",
  projectId: "instagram-clone-bb00e",
  storageBucket: "instagram-clone-bb00e.appspot.com",
  messagingSenderId: "777667763409",
  appId: "1:777667763409:web:fd3f70f19b090be14979f0",
  measurementId: "G-9KVCB1GK8R",
});
const db = firebaseApp.firestore();
const auth = firebase.auth();
const storage = firebase.storage();

export { db, auth, storage };
